<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MilesCarbonFootprint extends Model
{
    use HasFactory;
    protected $table = "miles_carbon_footprints";
    protected $fillable = ['activity','mode_type','country','carbonFootprint'];
}