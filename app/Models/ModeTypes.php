<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModeTypes extends Model
{
    use HasFactory;
    protected $table = "mode_types";
    protected $fillable = ['name'];
}