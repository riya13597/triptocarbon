<?php
namespace App\Repositories\TripToCarbon;



interface TripToCarbonInterface
{
    public function fuelCarbonFootprint($activity,$country,$fuel_type);

    public function milesCarbonFootprint($activity,$country,$mode_type);
}