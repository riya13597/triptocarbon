<?php
namespace App\Repositories\ApiCall;
use App\Repositories\ApiCall\ApiCallInterface;
use Illuminate\Support\Facades\Http;
use Config;

class ApiCall implements ApiCallInterface
{
    
/***********************************fuel footprint apicall***************************************/

    public function FuelParameterApiCall($activity,$country,$fuelType)
    {
        
       $response = Http::get(env("TRIP_TO_CARBON_URL"), [
                        'activity'     =>   $activity,
                        'activityType' =>   Config::get('activityType.activity_type.fuel'),
                        'country'      =>   $country,
                        'fuelType'     =>   $fuelType
        ]);
        
        return $response->json();
    }

/***********************************miles footprint apicall***************************************/

    public function MilesParameterApiCall($activity,$country,$mode_type)
    {
        $response = Http::get(env("TRIP_TO_CARBON_URL"), [
                        'activity'     =>   $activity,
                        'activityType' =>   Config::get('activityType.activity_type.miles'),
                        'country'      =>   $country,
                        'mode'         =>   $mode_type
        ]);
        
        return $response->json();
    }
}